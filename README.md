### **1. Docker Build**
### Se tiene que crear un fichero nuevo y poner todos los archivos ahi y parado en el fichero ejecutar
`docker build -t stacklamp .`


### **2. Docker run nginx para proxy reverso**
###   Se especifica nombre, puerto expuesto y volumen

`docker run -d --name reverseproxy -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy`


### **3. Docker run de nuestra imagen**
###   Se indica el nombre del contenedor, variables de entorno, volumen donde se aloja el archivo raiz del servidor web

`docker run -d --name stack-lamp -e APPSERVERNAME=practica2.istea -e APPALIAS=www.practica2.istea -e MYSQL_USER_PASSWORD=abc1234 -e MYSQL_DB_NAME=stacklamp -e VIRTUAL_HOST=practica2.istea -v /var/www/practica2:/var/www/html stacklamp`


### **4. host**
###   se tiene que cargar el ip del servidor y el nombre que se asigno para poder acceder a la pagina



