# Indicamos la imagen a utilizar de base
FROM ubuntu:latest

# Info acerca de la imagen
LABEL maintainer="Kevin Cohen"
LABEL version="1.0"
LABEL description="Stack LAMP dockerizado"
ARG DEBIAN_FRONTEND=noninteractive

# Actualizacion de paquetes, instalacion paquetes mysql y php
RUN apt update && apt install -y apache2 mysql-server php libapache2-mod-php php-mysql && apt clean && apt autoremove

#Se asignan variables de entorno
ENV APPSERVERNAME example.com
ENV APPALIAS www.example.com
ENV MYSQL_USER sluser
ENV MYSQL_USER_PASSWORD 1234
ENV MYSQL_DB_NAME stacklamp

# Creamos el directorio 'app' en el raiz del contenedor
RUN mkdir -p /app
# Copiamos el fichero de vhost a 'sites-available'
COPY default.conf /etc/apache2/sites-available
# Copiamos el directorio 'apache2' de 'etc' hacia '/app/apache2'
# Si hay un punto de montaje hacia el directorio 'apache2'
# El entrypoint al encontrar vacio al directorio va a traer los ficheros correspondientes de '/app/apache2'
RUN cp -r /etc/apache2 /app/apache2

#Se especifican los volumenes que va a utilizar el contenedor
VOLUME ["/var/www/html", "/var/lib/mysql", "/etc/apache2"]

#Copiar script de inicio al directorio raiz y dar permisos de ejecuciónse lo ejecuta
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
# Exponer puertos
EXPOSE 80
# Comando a ejecutarse cuando se crea el contenedor
ENTRYPOINT ["/entrypoint.sh"]

